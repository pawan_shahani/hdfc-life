package offline_test.sminq.com.hdfclife.utils

/**
 * Created by Pawan on 15/09/17.
 */

object AppConstants {

    //App level variables goes below...
    val APP_URL = "https://api.github.com/"
    val REPOSITORIES_URL = "repositories"


    ///////////........CREDENTIALS FOR HEADERS OF API CALLS.............\\\\\\\\\\\\\
    val USERNAME = "pawan.shahani@gmail.com"
    val PASSWORD = "Pawan@123"


    ///////////////...........VARIABLES FOR BUNDLE EXTRAS...........\\\\\\\\\\\\
    val CONTRIBUTIONS_URL_EXTRAS = "CONTRIBUTIONS_URL_EXTRAS"
    val LANGUAGES_URL_EXTRAS = "LANGUAGES_URL_EXTRAS"
    val BRANCHES_URL_EXTRAS = "BRANCHES_URL_EXTRAS"
    val CUSTOM_ACTIVITY_ACCESSED_VIA = "CUSTOM_ACTIVITY_ACCESSED_VIA"
    val CONTRIBUTIONS_ARRAY_EXTRAS = "CONTRIBUTIONS_ARRAY_EXTRAS"
    val BRANCHES_ARRAY_EXTRAS = "BRANCHES_ARRAY_EXTRAS"
    val DEVICE_ROTATE_REPOS_LIST_EXTRAS = "DEVICE_ROTATE_REPOS_LIST_EXTRAS"
    val DEVICE_ROTATE_REPOS_FILTER_BY_EXTRA = "DEVICE_ROTATE_REPOS_FILTER_BY_EXTRA"



    ////////////...........VARIABLES FOR API CALLS..........\\\\\\\\\\\\\\\\\\
    val GET_PUBLIC_REPOS_API_TAG = "GET_PUBLIC_REPOS_API_TAG"
    val CONTRIBUTORS_API_TAG = "CONTRIBUTORS_API_TAG"
    val BRANCHES_API_TAG = "BRANCHES_API_TAG"
    val LANGUAGES_API_TAG = "LANGUAGES_API_TAG"
}//AppConstants closes here....
