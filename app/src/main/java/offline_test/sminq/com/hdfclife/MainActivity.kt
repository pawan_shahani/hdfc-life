package offline_test.sminq.com.hdfclife

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import offline_test.sminq.com.hdfclife.adapters.PublicRepoAdapter
import offline_test.sminq.com.hdfclife.enums.REPO_FILTER_BY
import offline_test.sminq.com.hdfclife.network.NetworkResponseHandler
import offline_test.sminq.com.hdfclife.network.NetworkResponseListener
import offline_test.sminq.com.hdfclife.pojos.OpenRepoPOJO
import offline_test.sminq.com.hdfclife.utils.AppConstants
import org.json.JSONArray
import org.json.JSONException
import java.util.*


/**
 * Created by Pawan on 15/09/17.
 */

class MainActivity : AppCompatActivity(), NetworkResponseListener {

    private var mProgressBar: ProgressBar? = null
    private var mPublicRepoRecyclerV: RecyclerView? = null
    private var mAdapter: PublicRepoAdapter? = null
    private var layoutManger: LinearLayoutManager? = null
    private var mParentContainer: ConstraintLayout? = null


    var mRepoFilterBy: REPO_FILTER_BY = REPO_FILTER_BY.ALL//Initially all will be displed therefore it is initialized to ALL.
    var repoArrayList:ArrayList<OpenRepoPOJO.repoDetails>? = null
    var forkedRepoAl:ArrayList<OpenRepoPOJO.repoDetails>? = null


    //Least priority variables goes below....
    val TAG:String = "MainActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mProgressBar = findViewById<ProgressBar>(R.id.txtV)
        mPublicRepoRecyclerV = findViewById<RecyclerView>(R.id.publicRepoRecyclerV)
        mParentContainer = findViewById<ConstraintLayout>(R.id.activityMainParentContainer)


        if(savedInstanceState == null) {

            //Fetching the List of Repositories.....
            val fetchReponseUrl = AppConstants.APP_URL + AppConstants.REPOSITORIES_URL
            val getPublicRepos = NetworkResponseHandler(this@MainActivity, this@MainActivity,
                    fetchReponseUrl, null, mProgressBar!!, null, false)

            getPublicRepos.setNetworkResponseListener(this@MainActivity)
            getPublicRepos.executeGET(AppConstants.GET_PUBLIC_REPOS_API_TAG)

        }//if(savedInstanceState == null) closes here...
        else{
            //Set adapter directly using the data...
            repoArrayList = savedInstanceState.getSerializable(AppConstants.DEVICE_ROTATE_REPOS_LIST_EXTRAS) as ArrayList<OpenRepoPOJO.repoDetails>
            val filterBy:REPO_FILTER_BY = savedInstanceState.getSerializable(AppConstants.DEVICE_ROTATE_REPOS_FILTER_BY_EXTRA) as REPO_FILTER_BY

            when (filterBy){

                REPO_FILTER_BY.ALL -> {
                    setAdapter(repoArrayList!!)
                }//REPO_FILTER_BY.ALL closes here....

                REPO_FILTER_BY.FORKED -> {
                    //Lets filter & then set the Adapter...
                    setAdapter(getForkedArrayList()!!)
                }//REPO_FILTER_BY.FORKED closes here....
            }//when (filterBy) closes here...



        }//else closes here...

    }//onCreate closes here....


    /**This method will set the Adapter...**/
    private fun setAdapter(repoArrayList: ArrayList<OpenRepoPOJO.repoDetails>) {
        //Setting Adapter now....
        if (mAdapter == null)
            mAdapter = PublicRepoAdapter(this, repoArrayList, R.layout.single_row_public_repo)

        if (layoutManger == null)
            layoutManger = LinearLayoutManager(this, LinearLayout.VERTICAL, false)



        mPublicRepoRecyclerV!!.layoutManager = layoutManger
        mPublicRepoRecyclerV!!.setHasFixedSize(true)
        mPublicRepoRecyclerV!!.adapter = mAdapter


        mProgressBar!!.visibility = View.GONE
    }//setAdapter closes here...



    override fun networkResponseSuccess(response: String) {

        //Let's parse the response & set the Adapter.....
        repoArrayList = ArrayList<OpenRepoPOJO.repoDetails>()

        try {

            val responseArray = JSONArray(response)

            for (i in 0..responseArray.length() - 1) {

                //////////................PARSING REQUEST DETAILS.............\\\\\\\\\\\\\\\\\\
                val repoDetails = responseArray.getJSONObject(i)


                //////////////.............PARSING OWNER DETAILS..................\\\\\\\\\\\\\\\\
                val repoOwnerDetails = repoDetails.getJSONObject("owner")

                val repoDetailsPOJO = OpenRepoPOJO.repoDetails(
                        repoDetails.getInt("id"),
                        repoDetails.getString("name"),
                        repoDetails.getString("full_name"),

                        repoDetails.getString("description"),
                        repoDetails.getBoolean("fork"),

                        repoOwnerDetails.getString("login"),
                        repoOwnerDetails.getInt("id"),
                        repoOwnerDetails.getString("avatar_url"),

                        repoDetails.getString("contributors_url"),
                        repoDetails.getString("languages_url"),
                        repoDetails.getString("branches_url")
                )


                repoArrayList!!.add(repoDetailsPOJO)
            }//for loop closes here....


            //Now lets set the Adapter....
            if (mAdapter == null)
                mAdapter = PublicRepoAdapter(this, repoArrayList, R.layout.single_row_public_repo)

            if (layoutManger == null)
                layoutManger = LinearLayoutManager(this, LinearLayout.VERTICAL, false)



            mPublicRepoRecyclerV!!.layoutManager = layoutManger
            mPublicRepoRecyclerV!!.setHasFixedSize(true)
            mPublicRepoRecyclerV!!.adapter = mAdapter

        }//try closes here....
        catch (je: JSONException) {
            je.printStackTrace()
            Log.e(TAG, "Exception while parsing Public repository response: "+je)
        }
        //catch closes here....
    }//networkResponseSuccess closes here....


    override fun networkResponseFailure(errorMessage: String) {
        Snackbar.make(mParentContainer!!, errorMessage, Snackbar.LENGTH_SHORT).show()
    }//networkResponseFailure closes here....


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.repo_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }//onCreateOptionsMenu closes here....


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item!!.itemId) {

            R.id.action_filter -> {
                //Filter clicked...

                when(mRepoFilterBy){

                    REPO_FILTER_BY.ALL ->{
                        //Since currently the REPO is ALL, so lets display the FORKED data...
                        mRepoFilterBy = REPO_FILTER_BY.FORKED
                        forkedRepoAl = getForkedArrayList()

                        //Lets notify Adapter now....
                        mAdapter!!.setData(forkedRepoAl)
                        mAdapter!!.notifyDataSetChanged()
                    }//REPO_FILTER_BY.ALL closes here.....



                    REPO_FILTER_BY.FORKED ->{
                        //Since currently the REPO is FORKED, so lets display the Filtered data...
                        mRepoFilterBy = REPO_FILTER_BY.ALL

                        mAdapter!!.setData(repoArrayList)
                        mAdapter!!.notifyDataSetChanged()
                    }
                }//when(mRepoFilterBy) closes here....
            }//R.id.action_filter closes here....
        }//when(item) closes here....
        return super.onOptionsItemSelected(item)
    }//onOptionsItemSelected closes here....




    /**
     * This function will filter the List & sreturn the Forked List.**/
    private fun getForkedArrayList(): ArrayList<OpenRepoPOJO.repoDetails>? {

        forkedRepoAl = ArrayList<OpenRepoPOJO.repoDetails>()

        for (repoDetails in repoArrayList!!) {

            if (repoDetails.repoIsForked) {
                //Repo is Forked, so lets add in the new ArrayList...
                forkedRepoAl!!.add(repoDetails)
            }//if (repoDetails.repoIsForked) closes here....

        }//for each loop closes here....

        return forkedRepoAl
    }//getForkedArrayList closes here....


    override fun onSaveInstanceState(outState: Bundle?) {

        //Device is rotated, lets save array in Bundle...
        outState!!.putSerializable(AppConstants.DEVICE_ROTATE_REPOS_LIST_EXTRAS, repoArrayList)
        outState!!.putSerializable(AppConstants.DEVICE_ROTATE_REPOS_FILTER_BY_EXTRA, mRepoFilterBy)


        super.onSaveInstanceState(outState)
    }//onSaveInstanceState closes here...
}//MainActivity closes here....
