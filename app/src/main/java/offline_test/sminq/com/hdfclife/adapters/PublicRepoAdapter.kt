package offline_test.sminq.com.hdfclife.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import offline_test.sminq.com.hdfclife.MainActivity

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.R
import offline_test.sminq.com.hdfclife.RepoDetailsHandlerActivity
import offline_test.sminq.com.hdfclife.pojos.OpenRepoPOJO
import offline_test.sminq.com.hdfclife.utils.AppConstants

/**
 * Created by Pawan on 15/09/17.
 */

class PublicRepoAdapter(
        private var context: Context?,
        private var repoArrayList: ArrayList<OpenRepoPOJO.repoDetails>?,
        private val single_row_public_repo: Int)

    : RecyclerView.Adapter<PublicRepoAdapter.ViewHolder>() {


    //Least priority variables goes below....
    private val TAG = "PublicRepoAdapter"


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PublicRepoAdapter.ViewHolder {
        if (context == null)
            context = parent.context

        val view = LayoutInflater.from(context).inflate(single_row_public_repo, parent, false)
        return ViewHolder(view)
    }//onCreateViewHolder closes here....

    override fun onBindViewHolder(holder: PublicRepoAdapter.ViewHolder, position: Int) {

        ///////////...........DISPLAYING DATA ON THE UI...........\\\\\\\\\\\\\\
        Glide.with(context)
                .load(repoArrayList!![position].repoUserProfilePic)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .override(60,60)
                .into(holder.usrProfileImgV);




        ///////////////........SETTING DATA ON THE LAYOUT.........\\\\\\\\\\\\\\\\\
        holder.mRepoNameTxtV.text = repoArrayList!![position].repoName
        holder.mRepoDetailsTxtV.text = repoArrayList!![position].repoDescription

        var authorText = context!!.getString(R.string.bySpelling)+" "+repoArrayList!![position].repoUserName
        if(authorText != null)
            holder.mRepoAuthorNameTxtV.text = authorText





        if(repoArrayList!![position].repoIsForked){
            //if repo is forked, then we will display the Forked Icon.
            holder.mForkedImgV.visibility = View.VISIBLE
        }//if repo is forked closes here....
        else{
            //else repo is not forked then control goes here....
            holder.mForkedImgV.visibility = View.GONE
        }//else repo is forked closes here.....





        ///////////////..............REPO CLICK EVENT.............\\\\\\\\\\\\\\\
        holder.mRepoContainer.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {

                val intent = Intent(context, RepoDetailsHandlerActivity::class.java)
                intent.putExtra(AppConstants.BRANCHES_URL_EXTRAS, repoArrayList!![position].repoBranchesUrl)
                intent.putExtra(AppConstants.CONTRIBUTIONS_URL_EXTRAS, repoArrayList!![position].repoContributions)
                intent.putExtra(AppConstants.LANGUAGES_URL_EXTRAS, repoArrayList!![position].repoLanguagesUrl)
                (context as AppCompatActivity).startActivity(intent)
            }//onClick closes here....
        })//View.OnClickListener closes here....



    }//onBindViewHolder closes here....

    override fun getItemCount(): Int {
        if (repoArrayList != null)
            return repoArrayList!!.size
        else
            return 0
    }//getItemCount closes here....





    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val mRepoNameTxtV: TextView
        val mRepoDetailsTxtV: TextView
        val mRepoAuthorNameTxtV: TextView
        val mRepoContainer:CardView
        val mForkedImgV:ImageView
        val usrProfileImgV:ImageView

        init {

            mRepoNameTxtV = itemView.findViewById<TextView>(R.id.repoNameTxtV)
            mRepoDetailsTxtV = itemView.findViewById<TextView>(R.id.repoDetailsTxtV)
            mRepoAuthorNameTxtV = itemView.findViewById<TextView>(R.id.repoAuthorNameTxtV)
            mRepoContainer = itemView.findViewById<CardView>(R.id.repoSingleRowContainer)
            mForkedImgV = itemView.findViewById<ImageView>(R.id.forkedImgV)
            usrProfileImgV = itemView.findViewById(R.id.usrProfileImgV)
        }//ViewHolder construcotr closes here....

    }//ViewHolder closes here....

    fun setData(repoArrayList: ArrayList<OpenRepoPOJO.repoDetails>?) {
        this.repoArrayList = repoArrayList
    }
}//PublicRepoAdapter class closes here....
