package offline_test.sminq.com.hdfclife.Exceptions;

import android.util.Log;

/**
 * Created by Pawan on 28/11/16.
 */
public class UnhandledException{

    //Least priority variables goes below.....
    private final String TAG = "UnhandledException";

    public UnhandledException(String errorMsg) {
//        super(errorMsg);

        Log.e(TAG, "UnhandledException: "+errorMsg);

//        switch (AppConstants.buildGeneratedFor){
//
//            case DEV:
//                break;
//
//            case UAT:
//            case LIVE:
//                Crashlytics.logException(new Exception(errorMsg));
//                break;
//        }//switch (AppConstants.buildGeneratedFor) closes here....

    }//UnhandledException closes here....
}//UnhandledException closes here.....
