package offline_test.sminq.com.hdfclife.utils

import android.content.Context
import android.util.Log
import org.json.JSONObject

import offline_test.sminq.com.hdfclife.RepoDetailsHandlerActivity
import offline_test.sminq.com.hdfclife.interfaces.GetLanguagesListener
import offline_test.sminq.com.hdfclife.network.NetworkResponseHandler
import offline_test.sminq.com.hdfclife.network.NetworkResponseListener
import offline_test.sminq.com.hdfclife.pojos.LanguagesPOJO
import org.json.JSONException

/**
 * Created by Pawan on 16/09/17.
 */

class GetLanguagesResponseHandler(private val mContext: Context, private val url: String) : NetworkResponseListener {
    private var listener: GetLanguagesListener? = null


    //Least priority variables goes below...
    private val TAG = "GetLanguages"

    fun setListener(languagesListener: GetLanguagesListener) {
        this.listener = languagesListener
    }

    fun execute(languages_API_TAG: String) {
        val contributionsNetworkHandler = NetworkResponseHandler(mContext, this@GetLanguagesResponseHandler,
                url, null, null, null, false)
        contributionsNetworkHandler.setNetworkResponseListener(this@GetLanguagesResponseHandler)
        contributionsNetworkHandler.executeGET(languages_API_TAG)
    }

    override fun networkResponseSuccess(response: String) {

        try {
            val languageObject = JSONObject(response)
            val key = languageObject.keys().next()

            currentLangPOJO = LanguagesPOJO(key, languageObject.getInt(key))

            listener!!.languagesListSuccess(currentLangPOJO!!)
        }//try closes here....
        catch (je: JSONException) {
            je.printStackTrace()
            Log.e(TAG, "Exception while parsing Languages Response: "+je)
        }
        //catch closes here...
    }//networkResponseSuccess closes here.....

    override fun networkResponseFailure(errorMessage: String) {
        listener!!.languagesListFailure()
    }//networkResponseFailure closes here....

    companion object {
        var currentLangPOJO: LanguagesPOJO? = null
            private set
    }
}//GetLanguagesResponseHandler closes here....
