package offline_test.sminq.com.hdfclife.pojos

import java.io.Serializable

/**
 * Created by Pawan on 16/09/17.
 */
data class BranchesPOJO(val brnachName:String) : Serializable{
}