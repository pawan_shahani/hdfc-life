package offline_test.sminq.com.hdfclife.interfaces

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.pojos.ContributionsPOJO

/**
 * Created by Pawan on 16/09/17.
 */

interface GetContributionsListener {

    fun contributionsListSuccess(contributionAL: ArrayList<ContributionsPOJO>)

    fun contributionsListFailure()

}//GetContributionsListener closes here....
