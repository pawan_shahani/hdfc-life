package offline_test.sminq.com.hdfclife.enums

/**
 * Created by Pawan on 16/09/17.
 */
enum class DetailSelectionEnum {
    CONTRIBUTORS, LANGUAGES, BRANCHES
}//DetailSelectionEnum closes here...