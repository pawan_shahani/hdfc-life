package offline_test.sminq.com.hdfclife.pojos

import java.io.Serializable

/**
 * Created by Pawan on 16/09/17.
 */

data class ContributionsPOJO(val contributorName:String,
                             val contibutorID:Int,
                             val contributorProfilePic:String,
                             val contibutorHTMLProfile:String,
                             val totalContributions:Int) : Serializable {
}