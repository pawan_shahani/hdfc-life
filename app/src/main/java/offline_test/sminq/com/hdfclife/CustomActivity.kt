package offline_test.sminq.com.hdfclife

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.adapters.CustomAdapter
import offline_test.sminq.com.hdfclife.enums.DetailSelectionEnum
import offline_test.sminq.com.hdfclife.pojos.BranchesPOJO
import offline_test.sminq.com.hdfclife.pojos.ContributionsPOJO
import offline_test.sminq.com.hdfclife.utils.AppConstants

/**
 * Created by Pawan on 16/09/17.
 *
 * This Activity will control the Contributors & Branches listing
 */

class CustomActivity : AppCompatActivity() {

    //High priority UI variables goes below....
    private var mRecyclerV: RecyclerView? = null


    //Least priority variables goes below....
    private val TAG = "CustomActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mRecyclerV = findViewById(R.id.publicRepoRecyclerV)
        (findViewById<View>(R.id.txtV) as ProgressBar).visibility = View.GONE


        val adapter = CustomAdapter(this@CustomActivity)
        if (intent.hasExtra(AppConstants.CUSTOM_ACTIVITY_ACCESSED_VIA)) {

            val activityAccessedVia = intent.getSerializableExtra(AppConstants.CUSTOM_ACTIVITY_ACCESSED_VIA) as DetailSelectionEnum

            when (activityAccessedVia) {

                DetailSelectionEnum.CONTRIBUTORS -> {
                    val contributionsAl = intent.getSerializableExtra(AppConstants.CONTRIBUTIONS_ARRAY_EXTRAS) as ArrayList<ContributionsPOJO>
                    adapter.setAdapterAccessedVia(DetailSelectionEnum.CONTRIBUTORS)
                    adapter.setData(contributionsAl)
                }

                DetailSelectionEnum.LANGUAGES -> {
                }

                DetailSelectionEnum.BRANCHES -> {
                    val branchesAl = intent.getSerializableExtra(AppConstants.BRANCHES_ARRAY_EXTRAS) as ArrayList<BranchesPOJO>
                    adapter.setAdapterAccessedVia(DetailSelectionEnum.BRANCHES)
                    adapter.setData(branchesAl)
                }
            }//switch (activityAccessedVia) closes here....
        }//if Intent has Accessed Via...


        val layoutManager = LinearLayoutManager(this@CustomActivity)
        mRecyclerV!!.layoutManager = layoutManager
        mRecyclerV!!.setHasFixedSize(true)

        if (adapter != null)
            mRecyclerV!!.adapter = adapter

    }//onCreate closes here....

}//CustomActivity closes here....
