package offline_test.sminq.com.hdfclife.enums

/**
 * Created by Pawan on 16/09/17.
 */
enum class REPO_FILTER_BY {

    ALL, FORKED
}