package offline_test.sminq.com.hdfclife.pojos

import java.io.Serializable

/**
 * Created by Pawan on 15/09/17.
 */
class OpenRepoPOJO {

//    data class repoDetails(val repoID:Int,
//                           val repoName:String,
//                           val full_name:String,
//
//                           val login:String,
//                           val repoOwnerID:Int,
//                           val avatar_url:String,
//                           val gravatar_id:String,
//                           val url:String,
//                           val html_url:String,
//                           val followers_url:String,
//                           val following_url:String,
//                           val gists_url:String,
//                           val starred_url:String,
//                           val subscriptions_url: String,
//                           val organizations_url:String,
//                           val repos_url:String,
//                           val events_url:String,
//                           val received_events_url:String,
//                           val type:String,
//                           val site_admin:Boolean,
//                           val repoIsPvt:Boolean, val repoHTMLUrl:String,
//                           val repoDescription:String, val repoIsForked:Boolean,
//                           val repoURL:String, val repoForksUrl:String,
//                           val repoKeysUrl:String, val repoCollaborationUrl:String,
//                           val repoteams_url:String, val repohooks_url:String, val repoissue_events_url:String,
//                           val repoevents_url:String, val repoassignees_url:String, val repobranches_url:String,
//                           val repotags_url:String, val repoblobs_url:String,
//                           val repogit_tags_url:String, val repogit_refs_url:String, val repotrees_url:String,
//                           val repostatuses_url:String, val repolanguages_url:String, val repostargazers_url:String,
//                           val repocontributors_url:String, val reposubscribers_url:String, val reposubscription_url:String,
//                           val repocommits_url:String, val repoissue_comment_url:String, val repocontents_url:String,
//                           val repocompare_url:String, val repomerges_url:String, val repoarchive_url:String,
//                           val repodownloads_url:String, val repoissues_url:String, val repopulls_url:String,
//                           val repomilestones_url:String, val reponotifications_url:String, val repolabels_url:String,
//                           val reporeleases_url:String, val repodeployments_url:String)


    data class repoDetails(val repoID:Int,
                           val repoName:String,
                           val repoFullName:String,

                           val repoDescription:String,
                           val repoIsForked:Boolean,

                           val repoUserName:String,
                           val repoOwnerID:Int,
                           val repoUserProfilePic:String,
                           val repoContributions:String,
                           val repoLanguagesUrl:String,
                           val repoBranchesUrl:String) : Serializable

}//OpenRepoPOJO closes here....