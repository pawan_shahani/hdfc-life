package offline_test.sminq.com.hdfclife.interfaces

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.pojos.BranchesPOJO

/**
 * Created by Pawan on 16/09/17.
 */

interface GetBranchesListener {

    fun branchesListSuccess(branchAl: ArrayList<BranchesPOJO>)

    fun branchesListFailure()

}//GetContributionsListener closes here....
