package offline_test.sminq.com.hdfclife.utils

import android.content.Context
import android.util.Log
import org.json.JSONArray

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.interfaces.GetBranchesListener
import offline_test.sminq.com.hdfclife.network.NetworkResponseHandler
import offline_test.sminq.com.hdfclife.network.NetworkResponseListener
import offline_test.sminq.com.hdfclife.pojos.BranchesPOJO
import org.json.JSONException

/**
 * Created by Pawan on 16/09/17.
 */

class GetBranchesResponseHandler(private val mContext: Context, private var url: String?) : NetworkResponseListener {

    private var listener: GetBranchesListener? = null


    //Least priority variables goes below...
    private val TAG = "GetBranches"

    fun setListener(branchListener: GetBranchesListener) {
        this.listener = branchListener
    }

    fun execute(branches_API_TAG: String) {
        
        if (url!!.contains("{/branch}"))
            url = url!!.replace("{/branch}", "")


        val contributionsNetworkHandler = NetworkResponseHandler(mContext, this@GetBranchesResponseHandler,
                url!!, null, null, null, false)
        contributionsNetworkHandler.setNetworkResponseListener(this@GetBranchesResponseHandler)
        contributionsNetworkHandler.executeGET(branches_API_TAG)
    }

    override fun networkResponseSuccess(response: String) {

        branchAl = ArrayList()

        try {

            val branchesArray = JSONArray(response)
            var branchPOJO: BranchesPOJO
            for (i in 0..branchesArray.length() - 1) {
                val branchName = branchesArray.getJSONObject(i).getString("name")
                branchPOJO = BranchesPOJO(branchName)
                branchAl!!.add(branchPOJO)
            }


            listener!!.branchesListSuccess(branchAl!!)
        }//try closes here....
        catch (je: JSONException) {
            je.printStackTrace()
            Log.e(TAG, "Exception while parsing Branches response: "+je)
        }
        //catch closes here....
    }//networkResponseSuccess closes here....

    override fun networkResponseFailure(errorMessage: String) {
        listener!!.branchesListFailure()
    }//networkResponseFailure closes here....

    companion object {
        var branchAl: ArrayList<BranchesPOJO>? = null
            private set
    }
}
