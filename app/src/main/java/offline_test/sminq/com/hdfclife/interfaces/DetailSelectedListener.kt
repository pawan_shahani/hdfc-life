package offline_test.sminq.com.hdfclife.interfaces

import offline_test.sminq.com.hdfclife.enums.DetailSelectionEnum

/**
 * Created by Pawan on 16/09/17.
 */
interface DetailSelectedListener {
    fun selectedDetails(selectedDetail: DetailSelectionEnum)
}//DetailSelectedListener closes here...