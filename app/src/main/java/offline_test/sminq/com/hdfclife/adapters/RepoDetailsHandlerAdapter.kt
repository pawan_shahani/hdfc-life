package offline_test.sminq.com.hdfclife.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import offline_test.sminq.com.hdfclife.CustomActivity

import offline_test.sminq.com.hdfclife.R
import offline_test.sminq.com.hdfclife.interfaces.DetailSelectedListener
import offline_test.sminq.com.hdfclife.enums.DetailSelectionEnum
import offline_test.sminq.com.hdfclife.utils.AppConstants
import offline_test.sminq.com.hdfclife.utils.GetBranchesResponseHandler
import offline_test.sminq.com.hdfclife.utils.GetContributionsResponseHandler
import offline_test.sminq.com.hdfclife.utils.GetLanguagesResponseHandler

/**
 * Created by Pawan on 16/09/17.
 */

class RepoDetailsHandlerAdapter(
        private var mContext: Context?, private val single_row_repo_details_handler: Int)
            : RecyclerView.Adapter<RepoDetailsHandlerAdapter.Viewholder>() {

    //Medium priority variables goes below.....
    private val repoHandlerArray: Array<String>?
    private lateinit var detailSelectionListener:DetailSelectedListener

    //Least priority variables goes below.....
    private val TAG = "RepoDetailsHandlerAdapter".toString().trim { it <= ' ' }.substring(0, 23)


    init {

        repoHandlerArray = arrayOf(mContext!!.getString(R.string.contributorsSpelling),
                mContext!!.getString(R.string.languagesSpelling),
                mContext!!.getString(R.string.branchesSpelling))
    }//RepoDetailsHandlerAdapter constructor closes here....

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {

        if (mContext != null)
            mContext = parent.context

        val view = LayoutInflater.from(mContext).inflate(single_row_repo_details_handler, parent, false)

        return Viewholder(view)
    }//onCreateViewHolder closes here....

    override fun onBindViewHolder(holder: Viewholder, position: Int) {

        holder.mHandlerTxtV.text = repoHandlerArray!![position]

        holder.repoDetailsContainer.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {

                Log.d(TAG, "detailSelectionListener: "+detailSelectionListener)
                when (position) {
                    0 -> {
                        //Contributors are clicked....
                        if(GetContributionsResponseHandler.contributionAL != null) {
                            var contribIntent = Intent(mContext, CustomActivity::class.java)
                            contribIntent.putExtra(AppConstants.CUSTOM_ACTIVITY_ACCESSED_VIA, DetailSelectionEnum.CONTRIBUTORS)
                            contribIntent.putExtra(AppConstants.CONTRIBUTIONS_ARRAY_EXTRAS, GetContributionsResponseHandler.contributionAL)
                            (mContext as AppCompatActivity).startActivity(contribIntent)

                            detailSelectionListener.selectedDetails(DetailSelectionEnum.CONTRIBUTORS)
                        }//if data is not null closes here....
                    }

                    1 -> {
                        //Languages is clicked....
                        Log.d(TAG, "Languages Al: " + GetLanguagesResponseHandler.currentLangPOJO)
                        detailSelectionListener.selectedDetails(DetailSelectionEnum.LANGUAGES)
                    }

                    2 -> {
                        //Branches is clicked....
                        if(GetBranchesResponseHandler.branchAl != null) {
                            var branchesIntent = Intent(mContext, CustomActivity::class.java)
                            branchesIntent.putExtra(AppConstants.CUSTOM_ACTIVITY_ACCESSED_VIA, DetailSelectionEnum.BRANCHES)
                            branchesIntent.putExtra(AppConstants.BRANCHES_ARRAY_EXTRAS, GetBranchesResponseHandler.branchAl)
                            (mContext as AppCompatActivity).startActivity(branchesIntent)

                            detailSelectionListener.selectedDetails(DetailSelectionEnum.BRANCHES)
                        }//if data is not null closes here....
                    }
                }//when (position) closes here....
            }//onClick closes here....
        })//View.OnClickListener closes here....
    }//onBindViewHolder closes here....

    override fun getItemCount(): Int {
        if (repoHandlerArray == null)
            return 0
        else
            return repoHandlerArray.size
    }//getItemCount closes here....


    inner class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val mHandlerTxtV: TextView
        val repoDetailsContainer: CardView

        init {

            mHandlerTxtV = itemView.findViewById<TextView>(R.id.handlerNameTxtV)
            repoDetailsContainer = itemView.findViewById(R.id.repo_details_container)
        }//Viewholder constructor closes here....
    }//Viewholder class closes here....



    fun setDetailSelectionListener(detailSelectionListener: DetailSelectedListener) {
        this.detailSelectionListener = detailSelectionListener
    }//setDetailSelectionListener closes here....
}//RepoDetailsHandlerAdapter closes here....
