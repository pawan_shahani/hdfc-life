package offline_test.sminq.com.hdfclife.pojos

/**
 * Created by Pawan on 16/09/17.
 */
data class LanguagesPOJO (val languageName:String, val languageID:Int){
}