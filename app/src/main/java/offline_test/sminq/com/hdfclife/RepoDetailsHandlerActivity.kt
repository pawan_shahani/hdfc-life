package offline_test.sminq.com.hdfclife

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.adapters.RepoDetailsHandlerAdapter
import offline_test.sminq.com.hdfclife.interfaces.DetailSelectedListener
import offline_test.sminq.com.hdfclife.enums.DetailSelectionEnum
import offline_test.sminq.com.hdfclife.interfaces.GetBranchesListener
import offline_test.sminq.com.hdfclife.interfaces.GetContributionsListener
import offline_test.sminq.com.hdfclife.interfaces.GetLanguagesListener
import offline_test.sminq.com.hdfclife.network.NetworkResponseHandler
import offline_test.sminq.com.hdfclife.pojos.BranchesPOJO
import offline_test.sminq.com.hdfclife.pojos.ContributionsPOJO
import offline_test.sminq.com.hdfclife.pojos.LanguagesPOJO
import offline_test.sminq.com.hdfclife.utils.AppConstants
import offline_test.sminq.com.hdfclife.utils.GetBranchesResponseHandler
import offline_test.sminq.com.hdfclife.utils.GetContributionsResponseHandler
import offline_test.sminq.com.hdfclife.utils.GetLanguagesResponseHandler

/**
 * Created by Pawan on 16/09/17.
 */

class RepoDetailsHandlerActivity : AppCompatActivity(), DetailSelectedListener, GetBranchesListener, GetContributionsListener, GetLanguagesListener {

    //High priority UI variables goes below....
    private var mRepoDetailsHandlerRV: RecyclerView? = null
    private var mLanguageContainer: CardView? = null
    private var mLanguageName: TextView? =null


    //Medium priority NON-UI variables goes below...
    var repoLanguage:LanguagesPOJO? = null


    //Least priority variables goes below....
    private val TAG = "RepoDetailsHandlerActivity".toString().trim { it <= ' ' }.substring(0, 23)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_details_handler)

        mRepoDetailsHandlerRV = findViewById(R.id.repoDetailsHandlerRecyclerV)
        mLanguageContainer = findViewById(R.id.languageContainer)
        mLanguageContainer!!.visibility = View.GONE
        mLanguageName = findViewById(R.id.languageNameTtV)


        //Setting the Adapter.....
        val adapter = RepoDetailsHandlerAdapter(this, R.layout.single_row_repo_details_handler)
        adapter.setDetailSelectionListener(this@RepoDetailsHandlerActivity)
        val mLayoutmanager = LinearLayoutManager(this@RepoDetailsHandlerActivity)
        mRepoDetailsHandlerRV!!.layoutManager = mLayoutmanager
        mRepoDetailsHandlerRV!!.setHasFixedSize(true)
        mRepoDetailsHandlerRV!!.adapter = adapter



        ////////////.........lets get data from Intent & call all the API's..........\\\\\\\\\\\\\\
        val contributionsUrl = intent.getStringExtra(AppConstants.CONTRIBUTIONS_URL_EXTRAS)
        val languagesUrl = intent.getStringExtra(AppConstants.LANGUAGES_URL_EXTRAS)
        val branchesUrl = intent.getStringExtra(AppConstants.BRANCHES_URL_EXTRAS)


        //We are caching the whole response now.....
        val getBranches = GetBranchesResponseHandler(this@RepoDetailsHandlerActivity, branchesUrl)
        getBranches.setListener(this@RepoDetailsHandlerActivity)
        getBranches.execute(AppConstants.BRANCHES_API_TAG)


        val getContributors = GetContributionsResponseHandler(this@RepoDetailsHandlerActivity, contributionsUrl)
        getContributors.setListener(this@RepoDetailsHandlerActivity)
        getContributors.execute(AppConstants.CONTRIBUTORS_API_TAG)


        val getLanguages = GetLanguagesResponseHandler(this@RepoDetailsHandlerActivity, languagesUrl)
        getLanguages.setListener(this@RepoDetailsHandlerActivity)
        getLanguages.execute(AppConstants.LANGUAGES_API_TAG)
    }

    override fun selectedDetails(selectedDetail: DetailSelectionEnum) {

        when (selectedDetail) {

            DetailSelectionEnum.CONTRIBUTORS -> {mLanguageContainer!!.visibility = View.GONE
                //Setting Adapter...

            }

            DetailSelectionEnum.BRANCHES -> mLanguageContainer!!.visibility = View.GONE

            DetailSelectionEnum.LANGUAGES ->{ mLanguageContainer!!.visibility = View.VISIBLE
                mLanguageName!!.text = repoLanguage?.languageName
            }

        }//when (selectedDetail) closes here....
    }

    override fun branchesListSuccess(branchAl: ArrayList<BranchesPOJO>) {

    }

    override fun branchesListFailure() {

    }

    override fun contributionsListSuccess(contributionAL: ArrayList<ContributionsPOJO>) {

    }

    override fun contributionsListFailure() {

    }

    override fun languagesListSuccess(langPOJO: LanguagesPOJO) {
        repoLanguage = langPOJO


        if(mLanguageContainer!!.visibility == View.VISIBLE && mLanguageName?.visibility == View.VISIBLE)
            mLanguageName!!.text = repoLanguage?.languageName.toString().trim()
    }

    override fun languagesListFailure() {

    }


    /**Activity will stop here, so lets cancel all the Requests..**/
    override fun onStop() {

        NetworkResponseHandler.cancelRequests(AppConstants.BRANCHES_API_TAG, this@RepoDetailsHandlerActivity)
        NetworkResponseHandler.cancelRequests(AppConstants.CONTRIBUTORS_API_TAG, this@RepoDetailsHandlerActivity)
        NetworkResponseHandler.cancelRequests(AppConstants.LANGUAGES_API_TAG, this@RepoDetailsHandlerActivity)

        super.onStop()
    }//onStop closes here...
}
