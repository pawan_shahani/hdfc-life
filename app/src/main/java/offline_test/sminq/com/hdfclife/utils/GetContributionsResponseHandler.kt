package offline_test.sminq.com.hdfclife.utils

import android.content.Context
import android.util.Log
import org.json.JSONArray
import org.json.JSONObject

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.RepoDetailsHandlerActivity
import offline_test.sminq.com.hdfclife.interfaces.GetContributionsListener
import offline_test.sminq.com.hdfclife.network.NetworkResponseHandler
import offline_test.sminq.com.hdfclife.network.NetworkResponseListener
import offline_test.sminq.com.hdfclife.pojos.ContributionsPOJO
import org.json.JSONException

/**
 * Created by Pawan on 16/09/17.
 */

class GetContributionsResponseHandler(private val mContext: Context, private val url: String) : NetworkResponseListener {

    private var listener: GetContributionsListener? = null

    //Least priority variables goes below...
    private val TAG = "GetContributions"

    fun setListener(listener: GetContributionsListener) {
        this.listener = listener
    }


    fun execute(contributions_API_TAG: String) {
        val contributionsNetworkHandler = NetworkResponseHandler(mContext, this@GetContributionsResponseHandler,
                url, null, null, null, false)
        contributionsNetworkHandler.setNetworkResponseListener(this@GetContributionsResponseHandler)
        contributionsNetworkHandler.executeGET(contributions_API_TAG)
    }

    override fun networkResponseSuccess(response: String) {

        contributionAL = ArrayList()
        try {
            //Parsing response...

            val responseArray = JSONArray(response)
            var contributionPOJO: ContributionsPOJO
            for (i in 0..responseArray.length() - 1) {

                val contributionObject = responseArray.getJSONObject(i)

                val contributorName = contributionObject.getString("login")

                val contributorID = contributionObject.getInt("id")

                val contributorProfilePic = contributionObject.getString("avatar_url")

                val contributorHTMLProfile = contributionObject.getString("html_url")

                val totalContribution = contributionObject.getInt("contributions")


                contributionPOJO = ContributionsPOJO(contributorName, contributorID, contributorProfilePic, contributorHTMLProfile, totalContribution)
                contributionAL!!.add(contributionPOJO)
            }//for closes here....


            listener!!.contributionsListSuccess(contributionAL!!)
        }//try closes here....
        catch (je: JSONException) {
            je.printStackTrace()
            Log.e(TAG, "Exception while parsing Contributions Response: "+je.printStackTrace())
        }//catch closes here....
    }//networkResponseSuccess closes here....

    override fun networkResponseFailure(errorMessage: String) {
        listener!!.contributionsListFailure()
    }//networkResponseFailure closes here....

    companion object {
        var contributionAL: ArrayList<ContributionsPOJO>? = null
            private set
    }
}
