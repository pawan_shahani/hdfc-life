package offline_test.sminq.com.hdfclife.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView

import java.util.ArrayList

import offline_test.sminq.com.hdfclife.R
import offline_test.sminq.com.hdfclife.enums.DetailSelectionEnum
import offline_test.sminq.com.hdfclife.pojos.BranchesPOJO
import offline_test.sminq.com.hdfclife.pojos.ContributionsPOJO

/**
 * Created by Pawan on 16/09/17.
 *
 * This adapter is common for both Branches & Contributors
 */

class CustomAdapter(private var mContext: Context?)
    : RecyclerView.Adapter<CustomAdapter.Viewholder>() {


    private var adapterAccessedVia: DetailSelectionEnum? = null
    private var data: ArrayList<*>? = null

    private val VIEW_TYPE_CONTRIBUTORS = 0
    private val VIEW_TYPE_BRANCHES = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        if (mContext == null)
            mContext = parent.context

        val view = LayoutInflater.from(mContext).inflate(R.layout.single_row_custom, parent, false)

        return Viewholder(view)
    }//onCreateViewHolder closes here...

    override fun onBindViewHolder(holder: Viewholder, position: Int) {

        when (getItemViewType(position)) {

            VIEW_TYPE_CONTRIBUTORS -> {
                val (contributorName, _, _, contibutorHTMLProfile, totalContributions) = data!![position] as ContributionsPOJO

                val contributorString:String = mContext!!.getString(R.string.contributorNameSpelling).plus(": ").plus(contributorName)
                val contributionsString:String = "\n".plus(mContext!!.getString(R.string.totalContributionsSpelling)).plus(": ").plus(totalContributions)

                holder.mTitleTxtV.text = contributorString.plus(contributionsString)
                holder.mViewFullProfile.visibility = View.VISIBLE

                holder.mParentContainer.setOnClickListener {
                    //                        String url = "http://www.example.com";
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(contibutorHTMLProfile)
                    (mContext as AppCompatActivity).startActivity(i)
                }//onClick closes here....

            }//View.OnClickListener() closes here...

            VIEW_TYPE_BRANCHES -> {
                holder.mViewFullProfile.visibility = View.GONE

                val (brnachName) = data!![position] as BranchesPOJO
                holder.mTitleTxtV.text = mContext!!.getString(R.string.branchNameSpelling).plus(": ").plus(brnachName)

            }//VIEW_TYPE_BRANCHES closes here....
        }//switch (getItemViewType(position)) closes here....
    }//onBindViewHolder closes here....


    override fun getItemCount(): Int {
        return if (data != null)
            data!!.size
        else
            0
    }//getItemCount closes here....


    override fun getItemViewType(position: Int): Int {
        when (adapterAccessedVia) {
            DetailSelectionEnum.CONTRIBUTORS -> return VIEW_TYPE_CONTRIBUTORS

            DetailSelectionEnum.BRANCHES -> return VIEW_TYPE_BRANCHES

            else -> return -1
        }//switch (adapterAccessedVia) closes here....
    }//getItemViewType closes here....


    fun setAdapterAccessedVia(adapterAccessedVia: DetailSelectionEnum) {
        this.adapterAccessedVia = adapterAccessedVia
    }

    fun setData(data: ArrayList<*>) {
        this.data = data
    }

    inner class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var mTitleTxtV: TextView
        internal var mParentContainer: RelativeLayout
        internal var mViewFullProfile: TextView

        init {

            mTitleTxtV = itemView.findViewById(R.id.customTitleTxtV)
            mParentContainer = itemView.findViewById(R.id.customParentContainer)
            mViewFullProfile = itemView.findViewById(R.id.viewFullProfileTxtV)
        }//Viewholder constructor closes here....
    }//Viewholder class closes here....
}//CustomAdapter closes here....
