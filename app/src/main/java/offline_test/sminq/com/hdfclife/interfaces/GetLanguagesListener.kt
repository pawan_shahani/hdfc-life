package offline_test.sminq.com.hdfclife.interfaces

import offline_test.sminq.com.hdfclife.pojos.LanguagesPOJO

/**
 * Created by Pawan on 16/09/17.
 */

interface GetLanguagesListener {

    fun languagesListSuccess(langPOJO: LanguagesPOJO)

    fun languagesListFailure()

}//GetContributionsListener closes here....
